<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 28/01/2023
  Time: 11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.pack.spring_mvc.model.*" %>
<%@ page import="java.util.List" %>
<%
    List<Types> types = (List<Types>) request.getAttribute("types");
%>

<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles">
            <ol class="breadcrumb">
                <li><a>Actualité - Insertion</a></li>
            </ol>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Insérer de nouveau contenue</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-validation">
                            <form action="<%= request.getContextPath() %>/create-contenue" method="post" enctype="multipart/form-data" novalidate>
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="validationCustom01">Nom
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="nom" id="validationCustom01"  placeholder="Enter un nom..." required>
                                                <div class="invalid-feedback">
                                                    Enter un nom.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="validationCustom02">Descriptions <span
                                                    class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <textarea class="form-control" id="validationCustom02" rows="4" name="descriptions" required></textarea>
                                                </div>
                                                <div class="invalid-feedback">
                                                    Entrer un description.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="idTypes">Types
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <select onchange="displayDateFin()" class="default-select wide form-control" name="idTypes" id="idTypes"  required>
                                                    <option  data-display="Choisir un types">Choisir un types</option>
                                                    <% for (Types type : types ) { %>
                                                        <option value="<%= type.getIdTypes() %>"><%= type.getNomTypes() %></option>
                                                        <% } %>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Choisissez un type.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="validationCustom06">Date Debut
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="date" class="form-control" name="dateDebut" id="validationCustom06" required>
                                                <div class="invalid-feedback">
                                                    Entrer une date.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row" style="display: none" id="dateFin">
                                            <label class="col-lg-4 col-form-label" for="validationCustom07">Date Fin
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="date" class="form-control" name="dateFin" id="validationCustom07" value="1999-01-01">
                                                <div class="invalid-feedback">
                                                    Entrer une date.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="validationCustom08">Lieu
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="lieu" id="validationCustom08" placeholder="Enter un lieu..." required>
                                                <div class="invalid-feedback">
                                                    Entrer un lieu.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label class="col-lg-4 col-form-label" for="validationCustom09">Images
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <div class="form-file">
                                                    <input type="file" class="form-file-input form-control" id="validationCustom09" name="files" multiple="multiple" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <div class="col-lg-8 ms-auto">
                                                <input type="submit" class="btn btn-primary" value="Enregistrer"></input>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="navBarAdmin.jsp" %>
