<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 29/01/2023
  Time: 13:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="no-js" lang="en">


<!-- Mirrored from wp.alithemes.com/html/nest/demo/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Jul 2022 10:55:12 GMT -->
<head>
  <meta charset="utf-8" />
  <title>Actu.mg</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta property="og:title" content="" />

  <meta property="og:type" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="" />
  <!-- Favicon -->
  <!-- Template CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/user/css/plugins/slider-range.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/user/css/main17e6.css?v=5.2" />
</head>

<body>
<!-- Quick view -->

<header class="header-area header-style-1 header-height-2">
  <div class="header-middle header-middle-ptb-1 d-none d-lg-block">
    <div class="container">
      <div class="header-wrap">
        <div class="logo logo-width-1">
          <a><img src="${pageContext.request.contextPath}/resources/theme/logo/logo3.png" alt="logo"/></a>
        </div>
        <div class="header-right">
          <div class="search-style-2">
            <form action="<%= request.getContextPath() %>/recherche-user" method="post">
              <input type="mot" name="mot" placeholder="Rechercher ici..." />
            </form>
          </div>
          <div class="header-action-right">
            <div class="header-action-2">

              <div class="header-action-icon-2">
                <a class="mini-cart-icon">
                  <img alt="Nest" src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-cart.svg" />
                  <span class="pro-count blue">0</span>
                </a>
                <a><span class="lable">Panier</span></a>
              </div>
              <div class="header-action-icon-2">
                <a>
                  <img class="svgInject" alt="Nest" src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-user.svg" />
                </a>
                <a><span class="lable ml-0">Compte</span></a>
                <div class="cart-dropdown-wrap cart-dropdown-hm2 account-dropdown">
                  <ul>
                    <li><a><i class="fi fi-rs-user mr-10"></i>Mon Compte</a></li>
                    <li><a><i class="fi fi-rs-sign-out mr-10"></i>Se déconnecter</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header-bottom header-bottom-bg-color sticky-bar">
    <div class="container">
      <div class="header-wrap header-space-between position-relative">
        <div class="logo logo-width-1 d-block d-lg-none">
          <a><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/logo.svg" alt="logo" /></a>
        </div>
        <div class="header-nav d-none d-lg-flex">
          <div class="main-categori-wrap d-none d-lg-block">
            <a class="categories-button-active">
              <span class="fi-rs-apps"></span> <span class="et">Tous les types</span>
              <i class="fi-rs-angle-down"></i>
            </a>
            <div class="categories-dropdown-wrap categories-dropdown-active-large font-heading">
              <div class="d-flex categori-dropdown-inner">
                <ul>
                  <% for (Types type : types) { %>
                    <li>
                      <a><%= type.getNomTypes() %></a>
                    </li>
                  <% } %>

                </ul>
              </div>
            </div>
          </div>
          <div class="main-menu main-menu-padding-1 main-menu-lh-2 d-none d-lg-block font-heading">
            <nav>
              <ul>
                <li>
                  <a class="active" href="<%= request.getContextPath() %>/liste-user?pageId=1">Accueil</a>
                </li>
                <li>
                  <a>A propos</a>
                </li>
                <li>
                  <a>Contact</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="header-action-icon-2 d-block d-lg-none">
          <div class="burger-icon burger-icon-white">
            <span class="burger-icon-top"></span>
            <span class="burger-icon-mid"></span>
            <span class="burger-icon-bottom"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

