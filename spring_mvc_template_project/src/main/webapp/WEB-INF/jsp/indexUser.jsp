<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 29/01/2023
  Time: 11:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.pack.spring_mvc.model.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.pack.spring_mvc.model.V_Contenue" %>
<%
    List<V_Contenue> contenues = (List<V_Contenue>) request.getAttribute("contenues");
    List<V_Contenue> left = (List<V_Contenue>) request.getAttribute("leftContenues");
    int size = Integer.parseInt(String.valueOf(request.getAttribute("size")));
    int actif = Integer.parseInt(String.valueOf(request.getAttribute("actif")));
    List<Types> types = (List<Types>) request.getAttribute("types");
%>
<%@include file="header.jsp" %>

<main class="main">
    <div class="container mb-30">
        <div class="row flex-row-reverse">
            <div class="col-lg-4-5">

                <section class="product-tabs section-padding position-relative">
                    <div class="section-title style-2">
                        <h3>Liste des actualités</h3>
                        <ul class="nav nav-tabs links" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="nav-tab-one" type="button" role="tab" aria-controls="tab-one" aria-selected="true">Types</button>
                            </li>
                            <% for (Types type : types) { %>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="nav-tab-six" type="button" role="tab" aria-controls="tab-six" aria-selected="false"><%= type.getNomTypes() %></button>
                                </li>
                            <% } %>
                        </ul>
                    </div>


                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="tab-one" role="tabpanel" aria-labelledby="tab-one">
                            <div class="row product-grid-4">
                                <% for (V_Contenue contenue : contenues) { %>
                                    <div class="col-lg-1-5 col-md-4 col-12 col-sm-6">
                                        <div class="product-cart-wrap mb-30">
                                            <div class="product-img-action-wrap">
                                                <div class="product-img product-img-zoom">
                                                    <a href="shop-product-right.html">
                                                        <img style="object-fit: cover; object-position: center; height: 150px ; width:200px" class="default-img" src="${pageContext.request.contextPath}/images/<%= contenue.getImages() %>" alt="" />
                                                        <img style="object-fit: cover; object-position: center; height: 150px ; width:200px" class="hover-img" src="${pageContext.request.contextPath}/images/<%= contenue.getImages() %>" alt="" />
                                                    </a>
                                                </div>
                                                <div class="product-action-1">
                                                    <a aria-label="Voir détail" class="action-btn" data-bs-toggle="modal" data-bs-target="#quickViewModal"><i class="fi-rs-eye"></i></a>
                                                </div>

                                            </div>
                                            <div class="product-content-wrap">
                                                <div class="product-category">
                                                    <a><%= contenue.getNomTypes() %></a>
                                                </div>
                                                <h2><a><%= contenue.getNom() %></a></h2>
                                                <div class="product-rate-cover">
                                                    <div class="product-rate d-inline-block">
                                                        <div class="product-rating" style="width: 90%"></div>
                                                    </div>
                                                    <span class="font-small ml-5 text-muted"> (4.0)</span>
                                                </div>
                                                <div>
                                                    <% if(contenue.getIdTypes() == 1){ %>
                                                    <span class="font-small text-muted"> <a class="active">Le <%= contenue.getDateDebut() %> à <%= contenue.getLieu() %></a></span>
                                                    <% }else{ %>
                                                    <span class="font-small text-muted"><a class="active">Du <%= contenue.getDateDebut() %> au <%= contenue.getDateFin() %> à <%= contenue.getLieu() %></a></span>
                                                    <% } %>
                                                </div>
                                                <div>
                                                    <span class="font-small text-muted"><%= contenue.getDescriptions() %></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <% } %>
                            </div>
                        </div>
                        <div class="pagination-area mt-20 mb-20">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-start">
                                    <li class="page-item">
                                        <a class="page-link"><i class="fi-rs-arrow-small-left"></i></a>
                                    </li>
                                    <% for (int i=1; i<=size; i++) { %>
                                        <% if (i == actif) { %>
                                            <li class="page-item active">
                                                <a class="page-link" href="<%= request.getContextPath() %>/liste-user?pageId=<%= i %>"><%= i %></a>
                                            </li>
                                        <% }else{ %>
                                            <li class="page-item">
                                                <a class="page-link" href="<%= request.getContextPath() %>/liste-user?pageId=<%= i %>"><%= i %></a>
                                            </li>
                                        <% } %>
                                    <% } %>
                                    <li class="page-item">
                                        <a class="page-link"><i class="fi-rs-arrow-small-right"></i></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-1-5 primary-sidebar sticky-sidebar pt-30">

                <div class="sidebar-widget widget-category-2 mb-30">
                    <h5 class="section-title style-1 mb-30">Types</h5>
                    <ul>
                        <% for (Types type : types) { %>
                            <li>
                                <a><%= type.getNomTypes() %></a>
                            </li>
                        <% } %>

                    </ul>
                </div>

                <!-- Product sidebar Widget -->
                <div class="sidebar-widget product-sidebar mb-30 p-30 bg-grey border-radius-10">
                    <h5 class="section-title style-1 mb-30">Nouveaux Acualités</h5>
                    <% for (V_Contenue contenue : left) { %>
                        <div class="single-post clearfix">
                            <div class="image">
                                <img src="${pageContext.request.contextPath}/images/<%= contenue.getImages() %>" alt="#" />
                            </div>
                            <div class="content pt-10">
                                <h6><a><%= contenue.getNom() %></a></h6>
                                <p class="price mb-0 mt-5"><%= contenue.getNomTypes() %></p>
                                <div class="product-rate">
                                    <div class="product-rating" style="width: 60%"></div>
                                </div>
                                <div>
                                    <% if(contenue.getIdTypes() == 1){ %>
                                    <span class="font-small text-muted"> <a class="active">Le <%= contenue.getDateDebut() %> à <%= contenue.getLieu() %></a></span>
                                    <% }else{ %>
                                    <span class="font-small text-muted"><a class="active">Du <%= contenue.getDateDebut() %> au <%= contenue.getDateFin() %> à <%= contenue.getLieu() %></a></span>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                    <% } %>
                </div>
            </div>/images/
        </div>
    </div>
</main>

<%@include file="footer.jsp" %>

