<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 29/01/2023
  Time: 13:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="main">
  <section class="section-padding footer-mid">
    <div class="container pt-15 pb-20">
      <div class="row">
        <div class="col">
          <div class="widget-about font-md mb-md-3 mb-lg-3 mb-xl-0">
            <div class="logo mb-30">
              <a class="mb-15"><img src="${pageContext.request.contextPath}/resources/theme/logo/logo3.png" alt="logo" /></a>
              <p class="font-lg text-heading">Vous partage le meilleur.</p>
            </div>
            <ul class="contact-infor">
              <li><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-location.svg" alt="" /><strong>Adresse: </strong> <span>Nanisana, Antananarivo 101 - Madagascar</span></li>
              <li><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-contact.svg" alt="" /><strong>Contacts: </strong><span>+261 34 56 548 53 - +261 34 57 548 53</span></li>
              <li><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-email-2.svg" alt="" /><strong>Email: </strong><span><a class="__cf_email__" data-cfemail="2e5d4f424b6e604b5d5a004d4143">ActuMg@actuMg.com</a></span></li>
              <li><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-clock.svg" alt="" /><strong>Ouvertures: </strong><span>24/24h</span></li>
            </ul>
          </div>
        </div>
        <div class="footer-link-widget col">
          <h4 class="widget-title">Notre société</h4>
          <ul class="footer-list mb-sm-5 mb-md-0">
            <li><a>A propos</a></li>
            <li><a>Contact</a></li>
          </ul>
        </div>
        <div class="footer-link-widget col">
          <h4 class="widget-title">Compte</h4>
          <ul class="footer-list mb-sm-5 mb-md-0">
            <li><a>Inscription</a></li>
            <li><a>Mon compte</a></li>
          </ul>
        </div>
        <div class="footer-link-widget col">
          <h4 class="widget-title">Types</h4>
          <ul class="footer-list mb-sm-5 mb-md-0">
            <li><a>Articles</a></li>
            <li><a>Evènements</a></li>
          </ul>
        </div>
        <div class="footer-link-widget widget-install-app col">
          <h4 class="widget-title">Installer l'application</h4>
          <p class="wow fadeIn animated">Depuis App Store ou Google Play</p>
          <div class="download-app">
            <a class="hover-up mb-sm-2 mb-lg-0"><img class="active" src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/app-store.jpg" alt="" /></a>
            <a class="hover-up mb-sm-2"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/google-play.jpg" alt="" /></a>
          </div>
          <p class="mb-20">Mode de Paiement</p>
          <img class="wow fadeIn animated" src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/payment-method.png" alt="" />
        </div>
      </div>
    </div>
  </section>

  <div class="container pb-30">
    <div class="row align-items-center">
      <div class="col-12 mb-30">
        <div class="footer-bottom"></div>
      </div>
      <div class="col-xl-4 col-lg-6 col-md-6">
        <p class="font-sm mb-0">&copy; 2023</p>
      </div>
      <div class="col-xl-4 col-lg-6 text-center d-none d-xl-block">
        <div class="hotline d-lg-inline-flex mr-30">
          <img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/phone-call.svg" alt="hotline" />
          <p>+261 34 57 548 53 - +261 34 56 548 53<span>7/7j</span></p>
        </div>
      </div>
      <div class="col-xl-4 col-lg-6 col-md-6 text-end d-none d-md-block">
        <div class="mobile-social-icon">
          <h6>Suivez-nous</h6>
          <a href="#"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-facebook-white.svg" alt="" /></a>
          <a href="#"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-twitter-white.svg" alt="" /></a>
          <a href="#"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-instagram-white.svg" alt="" /></a>
          <a href="#"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-pinterest-white.svg" alt="" /></a>
          <a href="#"><img src="${pageContext.request.contextPath}/resources/theme/user/imgs/theme/icons/icon-youtube-white.svg" alt="" /></a>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Vendor JS-->
<script src="${pageContext.request.contextPath}/resources/theme/user/js/vendor/modernizr-3.6.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/vendor/jquery-3.6.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/vendor/jquery-migrate-3.3.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/vendor/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/slick.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/jquery.syotimer.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/wow.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/slider-range.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/perfect-scrollbar.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/magnific-popup.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/waypoints.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/counterup.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/jquery.countdown.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/images-loaded.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/isotope.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/scrollup.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/jquery.vticker-min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/jquery.theia.sticky.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/plugins/jquery.elevatezoom.js"></script>
<!-- Template  JS -->
<script src="${pageContext.request.contextPath}/resources/theme/user/js/main17e6.js?v=5.2"></script>
<script src="${pageContext.request.contextPath}/resources/theme/user/js/shop17e6.js?v=5.2"></script>
</body>


<!-- Mirrored from wp.alithemes.com/html/nest/demo/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Jul 2022 10:55:13 GMT -->
</html>

