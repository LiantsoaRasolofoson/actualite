<%@ page import="com.pack.spring_mvc.model.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.pack.spring_mvc.model.V_Contenue" %><%--
  Created by IntelliJ IDEA.
  User: aina
  Date: 2023-01-11
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  List<V_Contenue> contenues = (List<V_Contenue>) request.getAttribute("contenues");
  int size = Integer.parseInt(String.valueOf(request.getAttribute("size")));
  int actif = Integer.parseInt(String.valueOf(request.getAttribute("actif")));
%>
<div class="content-body">
  <div class="container-fluid">
    <div class="row page-titles">
      <ol class="breadcrumb">
        <li>Actualité - Liste</li>
      </ol>
    </div>

    <div class="row">
      <% for (V_Contenue contenue : contenues) { %>
        <div class="col-xl-3 col-lg-6 col-sm-6">
          <div class="card">
            <div class="card-body">
              <div class="new-arrival-product">
                <div class="new-arrivals-img-contnent">
                  <img style="object-fit: cover; object-position: center; height: 150px ; width:200px" class="img-fluid" src="${pageContext.request.contextPath}/images/<%= contenue.getImages() %>" alt="">
                </div>
                <br/>
                <div class="new-arrival-content position-relative">
                  <h4><a href="ecom-product-detail.html"><%= contenue.getNom() %></a></h4>
                  <p>Types: <span class="item"> <%= contenue.getNomTypes() %> </span></p>
                  <% if(contenue.getIdTypes() == 1){ %>
                    <p>Date: <span class="item"><%= contenue.getDateDebut() %></span> </p>
                  <% }else{ %>
                    <p>Date: <span class="item">Du <%= contenue.getDateDebut() %> au <%= contenue.getDateFin() %></span> </p>
                  <% } %>
                  <p>Lieu: <span class="item"><%= contenue.getLieu() %></span></p>
                  <p class="text-content"><%= contenue.getDescriptions() %></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <%  } %>
    </div>
    <div class="row">
      <nav>
        <ul class="pagination">
          <li class="page-item page-indicator">
            <a class="page-link" href="javascript:void(0)">
              <i class="la la-angle-left"></i></a>
          </li>
          <% for (int i=1; i<=size; i++) { %>
            <% if (i == actif) { %>
              <li class="page-item active">
                <a class="page-link" href="<%= request.getContextPath() %>/liste-contenue?pageId=<%= i %>"><%= i %></a>
              </li>
            <% }else{ %>
              <li class="page-item">
                <a class="page-link" href="<%= request.getContextPath() %>/liste-contenue?pageId=<%= i %>"><%= i %></a>
              </li>
            <% } %>
          <% } %>
          <li class="page-item page-indicator">
            <a class="page-link" href="javascript:void(0)">
              <i class="la la-angle-right"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>
<%@include file="navBarAdmin.jsp" %>
