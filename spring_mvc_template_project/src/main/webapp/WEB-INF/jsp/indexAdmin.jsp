<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 29/01/2023
  Time: 04:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="admin, dashboard" />
  <meta name="author" content="DexignZone" />
  <meta name="robots" content="index, follow" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Dompet : Payment Admin Template" />
  <meta property="og:title" content="Dompet : Payment Admin Template" />
  <meta property="og:description" content="Dompet : Payment Admin Template" />
  <meta property="og:image" content="social-image.png" />
  <meta name="format-detection" content="telephone=no">

  <!-- PAGE TITLE HERE -->
  <title>Actu.mg</title>

  <link href="${pageContext.request.contextPath}/resources/theme/assets/css/style.css" rel="stylesheet">


</head>

  <body class="vh-100">
    <div class="authincation h-100">
      <div class="container h-100">
        <div class="row justify-content-center h-100 align-items-center">
          <div class="col-md-6">
            <div class="authincation-content">
              <div class="row no-gutters">
                <div class="col-xl-12">
                  <div class="auth-form">
                    <div class="text-center mb-3">
                      <a><img width="330px" height="80px" src="${pageContext.request.contextPath}/resources/theme/logo/logo3.png" alt=""></a>
                    </div>
                    <h4 class="text-center mb-4">Login</h4>
                    <form action="<%= request.getContextPath() %>/login-validation" method="post">
                      <div class="mb-3">
                        <label class="mb-1"><strong>Email</strong></label>
                        <input type="email" name="email" class="form-control" value="liantsoarasolofoson@gmail.com">
                      </div>
                      <div class="mb-3">
                        <label class="mb-1"><strong>Mot De Passe</strong></label>
                        <input type="password" name="motDePasse" class="form-control" value="1234">
                      </div>
                      <div class="row d-flex justify-content-between mt-4 mb-2">
                        <div class="mb-3">
                          <div class="form-check custom-checkbox ms-1">
                            <input type="checkbox" class="form-check-input" id="basic_checkbox_1">
                            <label class="form-check-label" for="basic_checkbox_1">Me Rappeler</label>
                          </div>
                        </div>
                        <div class="mb-3">
                          <a>Mot de passe oublié?</a>
                        </div>
                      </div>
                      <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-block">Se Connecter</button>
                      </div>
                    </form>
                    <div class="new-account mt-3">
                      <p>Avez-vous un compte? <a class="text-primary">Inscription</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="${pageContext.request.contextPath}/resources/theme/assets/vendor/global/global.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/theme/assets/js/custom.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/theme/assets/js/dlabnav-init.js"></script>
    <script src="${pageContext.request.contextPath}/resources/theme/assets/js/styleSwitcher.js"></script>
  </body>

</html>
