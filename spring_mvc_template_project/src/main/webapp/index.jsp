<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/main.css">
    <title>Actu.mg</title>
</head>
<body>
    <p>
        <a href="<%= request.getContextPath() %>/login-admin">Back Office</a>
    </p>
    <p>
        <a href="<%= request.getContextPath() %>/liste-user?pageId=1">Front Office</a>
    </p>
</body>
</html>