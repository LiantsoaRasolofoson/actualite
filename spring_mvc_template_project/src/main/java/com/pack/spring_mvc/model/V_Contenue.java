package com.pack.spring_mvc.model;

import javax.persistence.*;
import java.sql.Date;
@Entity
@Table(name = "v_contenue")
public class V_Contenue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcontenue")
    private Integer idContenue;

    @Column(name = "nom")
    private String nom;

    @Column(name = "descriptions")
    private String descriptions;

    @Column(name = "idtypes")
    private Integer idTypes;
    @Column(name = "datedebut")
    private Date dateDebut;

    @Column(name = "datefin")
    private Date dateFin;

    @Column(name = "lieu")
    private String lieu;

    @Column(name = "nomtypes")
    private String nomTypes;

    @Column(name = "images")
    private String images;

    public Date getDateDebut() {
        return dateDebut;
    }

    public Integer getIdContenue() {
        return idContenue;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public String getLieu() {
        return lieu;
    }

    public String getNom() {
        return nom;
    }

    public Integer getIdTypes() {
        return idTypes;
    }

    public String getNomTypes() {
        return nomTypes;
    }

    public String getImages() {
        return images;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setIdContenue(Integer idContenue) {
        this.idContenue = idContenue;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setIdTypes(Integer idTypes) {
        this.idTypes = idTypes;
    }

    public void setNomTypes(String nomTypes) {
        this.nomTypes = nomTypes;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
