package com.pack.spring_mvc.model;

import javax.persistence.*;

@Entity
public class Types {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idtypes")
    private Integer idTypes;

    @Column(name = "nomtypes")
    private String nomTypes;

    public void setIdTypes(Integer idTypes) {
        this.idTypes = idTypes;
    }

    public void setNomTypes(String nomTypes) {
        this.nomTypes = nomTypes;
    }

    public Integer getIdTypes() {
        return idTypes;
    }

    public String getNomTypes() {
        return nomTypes;
    }
}
