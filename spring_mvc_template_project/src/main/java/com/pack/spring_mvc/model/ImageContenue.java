package com.pack.spring_mvc.model;

import javax.persistence.*;

@Entity
public class ImageContenue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name = "idimagecontenue")
    private Integer idImageContenue;

    @Column(name = "idcontenue")
    private Integer idContenue;

    @Column( name = "nomimage")
    private  String nomImage;

    public void setIdImageContenue(Integer idImageContenue) {
        this.idImageContenue = idImageContenue;
    }

    public void setIdContenue(Integer idContenue) {
        this.idContenue = idContenue;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }

    public Integer getIdImageContenue() {
        return idImageContenue;
    }

    public Integer getIdContenue() {
        return idContenue;
    }

    public String getNomImage() {
        return nomImage;
    }
}

