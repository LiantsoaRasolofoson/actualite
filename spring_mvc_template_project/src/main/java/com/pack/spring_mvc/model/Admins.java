package com.pack.spring_mvc.model;

import javax.persistence.*;

@Entity
public class Admins {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column( name = "idadmins")
    private Integer idAdmins;

    @Column( name = "email")
    private String email;

    @Column( name = "motdepasse")
    private String motDePasse;

    @Column( name = "nom")
    private String nom;


    public void setIdAdmins(Integer idAdmins) {
        this.idAdmins = idAdmins;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Integer getIdAdmins() {
        return idAdmins;
    }

    public String getEmail() {
        return email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
}
