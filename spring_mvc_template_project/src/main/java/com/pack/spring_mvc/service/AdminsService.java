package com.pack.spring_mvc.service;

import com.pack.spring_mvc.dao.HibernateDao;
import com.pack.spring_mvc.model.Admins;
import com.pack.spring_mvc.model.V_Contenue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminsService {

    @Autowired
    HibernateDao dao;

    public HibernateDao getDao() {
        return dao;
    }

    public void setDao(HibernateDao dao) {
        this.dao = dao;
    }

    public Admins login(Admins admin)throws Exception{
        List admins = dao.findWhere(admin);
        if( admins.size() != 0 ){
            return (Admins)admins.get(0);
        }
        return null;
    }

    public Admins getAdmins(){
        return dao.findById(Admins.class, 1);
    }


}
