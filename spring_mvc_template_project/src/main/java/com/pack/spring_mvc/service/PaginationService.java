package com.pack.spring_mvc.service;

import com.pack.spring_mvc.dao.HibernateDao;
import com.pack.spring_mvc.model.V_Contenue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaginationService {

    @Autowired
    HibernateDao dao;

    public HibernateDao getDao() {
        return dao;
    }

    public void setDao(HibernateDao dao) {
        this.dao = dao;
    }
    public int [] pagination(int nbAffiche, int pageId, int nbTotal) throws Exception{
        //int nbTotal = dao.findAll(new V_Contenue()).size();
        double s = (double)nbTotal/nbAffiche;
        int size = 0;
        if( s>(int)s  ){
            size = (int)s + 1;
        }
        else{
            size = (int)s;
        }
        if( pageId != 1){
            pageId = pageId-1;
            pageId = pageId*nbAffiche+1;
        }
        int [] values = new int[2];
        values[0] = size;
        values[1] = pageId-1;
        return values;
    }
}
