package com.pack.spring_mvc.service;


import com.pack.spring_mvc.dao.HibernateDao;
import com.pack.spring_mvc.model.Admins;
import com.pack.spring_mvc.model.Contenue;
import com.pack.spring_mvc.model.ImageContenue;
import com.pack.spring_mvc.model.V_Contenue;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@Service
public class ContenueService {

    @Autowired
    HibernateDao dao;

    public HibernateDao getDao() {
        return dao;
    }

    public void setDao(HibernateDao dao) {
        this.dao = dao;
    }

    public List<V_Contenue> simpleListe(int offset, int limit) {
        return dao.paginate(new V_Contenue(), offset, limit);
    }

    public List<V_Contenue> recherche(int offset, int limit, String mot) {
        return dao.findPaginate(new V_Contenue(), offset, limit, mot);
    }

    public int countSimpleListe(){
        List liste = dao.findAll(new V_Contenue());
        return liste.size();
    }

    public int countRechercheListe(String mot){
        List liste = dao.findRecherche(new V_Contenue(), mot);
        return liste.size();
    }

    public void saveImage(MultipartFile[] files, Contenue contenue, String path)throws Exception{
        for (MultipartFile file : files) {
            if (!file.getOriginalFilename().isEmpty())
            {
                BufferedOutputStream outputStream = new BufferedOutputStream(
                        new FileOutputStream(new File(path, file.getOriginalFilename()))
                        //new FileOutputStream(new File("F:\\SpringMVC\\MrNaina\\Examen1\\BackOffice\\spring_mvc_template_project\\src\\main\\webapp\\resources\\theme\\assets\\images\\contenue", file.getOriginalFilename()))
                );
                outputStream.write(file.getBytes());
                outputStream.flush();
                outputStream.close();
                ImageContenue img = new ImageContenue();
                img.setIdContenue(contenue.getIdContenue());
                img.setNomImage(file.getOriginalFilename());
                dao.create(img);
            }
        }
    }

    public Contenue createContenue(Contenue contenue, MultipartFile [] files, String path)throws Exception{
        try{
            if( String.valueOf(contenue.getDateFin()).equals("1999-01-01") == true ){
                contenue.setDateFin(null);
            }
            Contenue c = dao.create(contenue);
            this.saveImage(files, c, path);
            return c;
        }
        catch (Exception e){
            throw e;
        }
    }
}
