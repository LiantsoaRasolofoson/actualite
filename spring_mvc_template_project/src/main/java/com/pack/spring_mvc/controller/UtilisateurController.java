package com.pack.spring_mvc.controller;

import com.pack.spring_mvc.dao.HibernateDao;
import com.pack.spring_mvc.model.Admins;
import com.pack.spring_mvc.model.Types;
import com.pack.spring_mvc.model.V_Contenue;
import com.pack.spring_mvc.service.ContenueService;
import com.pack.spring_mvc.service.PaginationService;
import com.pack.spring_mvc.service.TypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UtilisateurController {

    @Autowired
    PaginationService paginationService;

    @Autowired
    ContenueService contenueService;

    @Autowired
    private TypesService typesService;

    @GetMapping("/liste-user")
    public String listeContenue(Model model, @RequestParam("pageId") int pageId)throws Exception{
        try{
            int nbAffiche = 10;
            int [] pagination = paginationService.pagination(nbAffiche,pageId, contenueService.countSimpleListe());
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",pageId);
            model.addAttribute("types", typesService.listes());
            int nb = 0;
            model.addAttribute("leftContenues", contenueService.simpleListe(nb, 5));
            model.addAttribute("contenues", contenueService.simpleListe(pagination[1], nbAffiche));
            return "indexUser";
        }
        catch (Exception e){
            throw e;
        }
    }

    @PostMapping("/recherche-user")
    public String rechercher(@RequestParam String mot, Model model)throws Exception{
        int nbAffiche = 10;
        int pageId = 1;
        int [] pagination = paginationService.pagination(nbAffiche,pageId, contenueService.countRechercheListe(mot));
        model.addAttribute("size",pagination[0]);
        model.addAttribute("actif",pageId);
        model.addAttribute("mot", mot);
        model.addAttribute("types", typesService.listes());
        int nb = 0;
        model.addAttribute("leftContenues", contenueService.simpleListe(nb, 5));
        model.addAttribute("contenues", contenueService.recherche(pagination[1], nbAffiche, mot));
        return "recherce-result";
    }

    @GetMapping("/recherche-result")
    public String resultRecherche(Model model, @RequestParam("pageId") int pageId, @RequestParam("mot") String mot)throws Exception{
        try{
            int nbAffiche = 10;
            int [] pagination = paginationService.pagination(nbAffiche,pageId, contenueService.countRechercheListe(mot));
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",pageId);
            model.addAttribute("mot", mot);
            model.addAttribute("types", typesService.listes());
            int nb = 0;
            model.addAttribute("leftContenues", contenueService.simpleListe(nb, 5));
            model.addAttribute("contenues", contenueService.recherche(pagination[1], nbAffiche, mot));
            return "recherce-result";
        }
        catch (Exception e){
            throw e;
        }
    }
}
