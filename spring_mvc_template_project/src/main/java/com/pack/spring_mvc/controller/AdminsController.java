package com.pack.spring_mvc.controller;

import com.pack.spring_mvc.dao.HibernateDao;
import com.pack.spring_mvc.model.Admins;
import com.pack.spring_mvc.model.Contenue;
import com.pack.spring_mvc.model.V_Contenue;
import com.pack.spring_mvc.service.AdminsService;
import com.pack.spring_mvc.service.ContenueService;
import com.pack.spring_mvc.service.PaginationService;
import com.pack.spring_mvc.service.TypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

@Controller
public class AdminsController {

    @Autowired
    AdminsService adminsService;

    @Autowired
    PaginationService paginationService;

    @Autowired
    ContenueService contenueService;

    @Autowired
    private TypesService typesService;

    @GetMapping("/login-admin")
    public String loginAdmin(Model model)throws Exception{
        return "indexAdmin";
    }

    @PostMapping("/login-validation")
    public String validationLogin(@ModelAttribute Admins admin, Model model)throws Exception{
        String s = "indexAdmin";
        Admins admins = adminsService.login(admin);
        if( admins != null ){
            int nbAffiche = 8;
            int [] pagination = paginationService.pagination(nbAffiche,1, contenueService.countSimpleListe());
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",1);
            model.addAttribute("admins",admins);
            model.addAttribute("contenues", contenueService.simpleListe(pagination[1], nbAffiche));
            s = "liste-contenue";
        }
        return s;
    }

    @GetMapping("/form-contenue")
    public String formulaireContenue(Model model)throws Exception{
        model.addAttribute("admins",adminsService.getAdmins());
        model.addAttribute("types", typesService.listes() );
        return "formulaireContenue";
    }

    @PostMapping("/create-contenue")
    public String createContenue(@ModelAttribute Contenue contenue, @RequestParam("files") MultipartFile[] files, Model model, HttpSession session)throws Exception{
        try{
            String path = session.getServletContext().getRealPath("/images");
            Contenue c = contenueService.createContenue(contenue,files, path);
            int nbAffiche = 8;
            int [] pagination = paginationService.pagination(nbAffiche,1,  contenueService.countSimpleListe());
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",1);
            model.addAttribute("admins", adminsService.getAdmins());
            model.addAttribute("contenues", contenueService.simpleListe(pagination[1], nbAffiche));
            return "liste-contenue";
        }
        catch (Exception e){
            throw e;
        }
    }

    @GetMapping("/liste-contenue")
    public String listeContenue(Model model, @RequestParam("pageId") int pageId)throws Exception{
        try{
            int nbAffiche = 8;
            int [] pagination = paginationService.pagination(nbAffiche,pageId,  contenueService.countSimpleListe());
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",pageId);
            model.addAttribute("admins",adminsService.getAdmins());
            model.addAttribute("contenues", contenueService.simpleListe(pagination[1], nbAffiche));
            return "liste-contenue";
        }
        catch (Exception e){
            throw e;
        }
    }

    @PostMapping("/admin-recherche")
    public String rechercher(@RequestParam String mot, Model model)throws Exception{
        int nbAffiche = 8;
        int pageId = 1;
        int [] pagination = paginationService.pagination(nbAffiche,pageId, contenueService.countRechercheListe(mot));
        model.addAttribute("size",pagination[0]);
        model.addAttribute("actif",pageId);
        model.addAttribute("admins", adminsService.getAdmins());
        model.addAttribute("mot", mot);
        model.addAttribute("contenues", contenueService.recherche(pagination[1], nbAffiche, mot));
        return "result-recherche";
    }

    @GetMapping("/result-recherche")
    public String resultRecherche(Model model, @RequestParam("pageId") int pageId, @RequestParam("mot") String mot)throws Exception{
        try{
            int nbAffiche = 8;
            int [] pagination = paginationService.pagination(nbAffiche,pageId, contenueService.countRechercheListe(mot));
            model.addAttribute("size",pagination[0]);
            model.addAttribute("actif",pageId);
            model.addAttribute("admins", adminsService.getAdmins());
            model.addAttribute("mot", mot);
            model.addAttribute("contenues", contenueService.recherche(pagination[1], nbAffiche, mot));
            return "result-recherche";
        }
        catch (Exception e){
            throw e;
        }
    }

}
