package com.pack.spring_mvc.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.List;

public class HibernateDao {

    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <T> T create(T entity){
        Session session = null;
        Transaction transaction = null;
        try{
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( transaction.isActive() ){
                transaction.rollback();
            }
            if( session != null ){
                session.close();
            }
        }
        return entity;
    }

    public <T> T findById(Class<T> clazz, Serializable id){
        Session session = sessionFactory.openSession();
        T entity = (T) session.get(clazz, id);
        session.close();
        return entity;
    }

    public <T> List<T> findAll(T entity){
        Session session = null;
        Transaction transaction = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM "+entity.getClass().getSimpleName());
            list = query.list();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( transaction.isActive() ){
                transaction.rollback();
            }
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public <T> List<T> findWhere(T entity){
        Session session = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            Example example = Example.create(entity).ignoreCase();
            list = session.createCriteria(entity.getClass()).add(example).list();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public <T> List<T> findPaginate(T entity, int offset, int limit, String mot){
        Session session = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            list = session.createCriteria(entity.getClass())
                    .add(
                            Restrictions.or(
                                    Restrictions.ilike("nom", mot, MatchMode.ANYWHERE),
                                    Restrictions.ilike("descriptions", mot, MatchMode.ANYWHERE)
                            )
                    )
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .list();
            ;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public <T> List<T> findRecherche(T entity, String mot){
        Session session = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            list = session.createCriteria(entity.getClass())
                    .add(
                            Restrictions.or(
                                    Restrictions.ilike("nom", mot, MatchMode.ANYWHERE),
                                    Restrictions.ilike("descriptions", mot, MatchMode.ANYWHERE)
                            )
                    ).list();
            ;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public <T> List<T> paginateWhere (T entity, int offset, int limit){
        Session session = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            Example example = Example.create(entity).ignoreCase();
            list = session.createCriteria(entity.getClass())
                    .add(example)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .list();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public <T> List<T> paginate(T entity, int offset, int limit){
        Session session = null;
        List <T> list = null;
        try{
            session = this.sessionFactory.openSession();
            list = session.createCriteria(entity.getClass())
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .list();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( session != null ){
                session.close();
            }
        }
        return list;
    }

    public void delete(Object entity){
        Session session = null;
        Transaction transaction = null;
        try{
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( transaction.isActive() ){
                transaction.rollback();
            }
            if( session != null ){
                session.close();
            }
        }
    }

    public <T> T update(T entity){
        Session session = null;
        Transaction transaction = null;
        try{
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(entity);
            session.getTransaction().commit();
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if( transaction.isActive() ){
                transaction.rollback();
            }
            if( session != null ){
                session.close();
            }
        }
        return entity;
    }
}
